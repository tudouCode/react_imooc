//mockjs 文档的github地址: https://github.com/nuysoft/Mock/wiki
import Mock from "mockjs";
// 基础表格数据
Mock.mock("/list", "get", {
  success: true,
  message: "操作成功",
  "list|1-20": [
    {
      "id|+1": 1,
      userName: "@name",
      sex: "@natural(0,1)",
      time: "@time('HH:mm')", //随机的时间字符串,
      state: "@natural(1, 5)", //大于等于零的整数
      interest: "@integer(1,8)", //随机整数
      birthday: "@date(yyyy-MM-dd)",
      address: "@province" + "@city", //随机浮点数,
    },
  ],
});
// 高级表格数据
Mock.mock("/high/TableList", "get", {
  success: true,
  message: "操作成功",
  "list|1-20": [
    {
      "id|+1": 1,
      userName: "@name",
      sex: "@natural(0,1)",
      time: "@time('HH:mm')", //随机的时间字符串,
      state: "@natural(1, 5)", //大于等于零的整数
      interest: "@integer(1,8)", //随机整数
      birthday: "@date(yyyy-MM-dd)",
      address: "@province" + "@city", //随机浮点数,
    },
  ],
});

// 城市管理
Mock.mock("/city/TableList", "get", {
  success: true,
  message: "操作成功",
  "list|1-20": [
    {
      "id|+1": 1,
      name: "@city",
      mode: "@natural(0,1)",
      op_mode: "@natural(0,1)",
      auth_status: "@natural(0,1)",
      city_admins: "@name",
      sys_user_name: "@cname",
      open_time: "@date(yyyy-MM-dd)",
      update_time: "@date(yyyy-MM-dd)",
    },
  ],
});

// 订单管理

Mock.mock("/order/list", "get", {
  success: true,
  message: "操作成功",
  "list|1-20": [
    {
      "order_sn|+1": 1,
      bike_sn: "@natural(1, 1000)",
      user_name: "@cname",
      mobile: "@zip",
      distance: "@integer(0)",
      total_time: "@integer(0)",
      status: "@natural(0, 1)",
      start_time: "@date(yyyy-MM-dd)",
      end_time: "@date(yyyy-MM-dd)",
      total_fee: "@float(1, 100, 3, 6)",
      user_pay: "@float(1, 100, 3, 6)",
    },
  ],
});
