import React from "react";
import { Card, Button, message } from "antd";
import "./ui.less";
export default class loadings extends React.Component {
  state = {
    loading: false,
  };
  showMsg = (type) => {
    message[type]("弹出详细内容！");
  };
  render() {
    return (
      <div>
        <Card title="全局提示框" className="card-wrap">
          <Button type="primary" onClick={() => this.showMsg("success")}>
            Success
          </Button>
        </Card>
      </div>
    );
  }
}
