import React from "react";
import { Card, Tabs, message } from "antd";
import "./ui.less";
import { PlusCircleOutlined,EditOutlined,DeleteOutlined } from "@ant-design/icons";
const { TabPane } = Tabs;
export default class loadings extends React.Component {
  newTabIndex = 0;
  handleCallback = (key) => {
    message.info("切换标签" + key);
  };
  componentWillMount() { 
    const panes = [
      {
        title:'Tab 1',
        content: 'Tab 1',
        key:'1'
    },
    {
        title: 'Tab 2',
        content: 'Tab 2',
        key: '2'
    },
    {
        title: 'Tab 3',
        content: 'Tab 3',
        key: '3'
    }
    ]
    this.setState({
      activeKey: panes[0].key,
      panes
    })
  }
  onChange=(activekey)=> { 
    this.setState({
      activekey
    })
  }
  onEdit = (targetKey, action) => { 
    this[action](targetKey);
  }
  add = () => {
    const panes = this.state.panes
    const activeKey = `newTab${this.newTabIndex++}`
    panes.push({
      title: activeKey,
      content: 'new tab Panel',
      key:activeKey
    })
    this.setState({ panes, activeKey });
  }
  remove = (targetKey) => { 
    let { activeKey } = this.state;
    let lastIndex;
    this.state.panes.forEach((pane, i) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const panes = this.state.panes.filter(pane => pane.key !== targetKey);
    if (panes.length && activeKey === targetKey) {
      if (lastIndex >= 0) {
        activeKey = panes[lastIndex].key;
      } else {
        activeKey = panes[0].key;
      }
    }
    this.setState({ panes, activeKey });
  }
  render() {
    return (
      <div>
        <Card title="Tab页签" className="card-wrap">
          <Tabs defaultActiveKey="1" onChange={this.handleCallback}>
            <TabPane
              tab={<span><PlusCircleOutlined />tab1</span>}  key="1"  >
              001
            </TabPane>
            <TabPane
              tab={<span><EditOutlined/>tab1</span>}  key="2"  >
              001
            </TabPane>
            <TabPane
              tab={<span><DeleteOutlined />tab1</span>}  key="3"  >
              001
            </TabPane>
          </Tabs>
        </Card>
        <Card title="Tab页签" className="card-wrap">
          <Tabs defaultActiveKey="1" onChange={this.onChange} type="editable-card" onEdit={this.onEdit}>
            {this.state.panes.map((panel) => { 
              return <TabPane key={panel.key} tab={panel.title}></TabPane>
            })}
            </Tabs>
          </Card>
      </div>
    );
  }
}
