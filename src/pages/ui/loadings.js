import React from "react";
import { Card, Spin, Alert, Switch } from "antd";
import "./ui.less";
export default class loadings extends React.Component {
  state = {
    loading: false,
  };
  toggle = (type) => {
    this.setState({
      loading: type,
    });
  };

  render() {
    return (
      <div>
        <Card title="内容遮罩" className="card-wrap">
          <Spin spinning={this.state.loading}>
            <Alert
              message="React"
              description="欢迎来到React高级实战课程"
              type="info"
              style={{ marginBottom: 10 }}
            />
          </Spin>
          <div style={{ marginTop: 16 }}>
            状态切换：
            <Switch checked={this.state.loading} onChange={this.toggle} />
          </div>
        </Card>
      </div>
    );
  }
}
