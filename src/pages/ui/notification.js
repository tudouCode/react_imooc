import React from "react";
import { Button, notification } from "antd";
import "./ui.less";
export default class loadings extends React.Component {
  state = {
    loading: false,
  };
  render() {
    const openNotification = () => {
      notification.open({
        nmessage: "提示框信息",
        description: "详细信息描述",
        onClick: () => {
          console.log("回调函数");
        },
      });
    };
    return (
      <div>
        <Button type="primary" onClick={openNotification}>
          notification信息提示
        </Button>
      </div>
    );
  }
}
