import React from "react";
import { Card, Button, Modal } from "antd";

import "./ui.less";
export default class Buttons extends React.Component {
  state = {
    showModal1: false,
    showModal2: false,
  };
  handleOpen = (type) => {
    this.setState({
      [type]: true,
    });
  };
  handleConfirm = (type) => {
    console.log(type);
    Modal[type]({
      title: "确认？",
      content: "你确定关闭弹出框吗?",
      onOk() {
        console.log("Ok");
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  };
  render() {
    return (
      <div className="homeContaier">
        <Card title="基础模态框" className="card-wrap">
          <Button type="primary" onClick={() => this.handleOpen("showModal1")}>
            open
          </Button>
          <Button type="primary" onClick={() => this.handleConfirm("success")}>
            Confirm
          </Button>
        </Card>
        <Modal
          title="react学习demo"
          visible={this.state.showModal1}
          onCancel={() => {
            this.setState({
              showModal1: false,
            });
          }}
          onOk={() => {
            this.setState({
              showModal1: false,
            });
          }}
        >
          <p>欢迎学习慕课新推出的React高级课程</p>
        </Modal>
      </div>
    );
  }
}
