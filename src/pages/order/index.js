import React from "react";
import { Card, Button, Table, Modal, Form } from "antd";
import axios from "axios";
import BaseForm from "../../components/baseForm";
import "./../../mock";
const FormItem = Form.Item;
export default class Order extends React.Component {
  state = {
    orderInfo: {},
    orderConfirmVisble: false,
  };
  params = {
    page: 1,
  };
  formList = [
    {
      type: "SELECT",
      label: "城市",
      field: "city",
      placeholder: "全部",
      initialValue: "1",
      width: 80,
      name: "city",
      list: [
        { id: "0", name: "全部" },
        { id: "1", name: "北京" },
        { id: "2", name: "天津" },
        { id: "3", name: "上海" },
      ],
    },
    {
      type: "时间查询",
      name: "searchTime",
    },
    {
      type: "SELECT",
      label: "订单状态",
      field: "order_status",
      placeholder: "全部",
      initialValue: "1",
      width: 120,
      list: [
        { id: "0", name: "全部" },
        { id: "1", name: "进行中" },
        { id: "2", name: "结束行程" },
      ],
    },
  ];
  componentDidMount() {
    this.requestList();
  }
  handleFilter = (params) => {
    this.params = params;
    this.requestList();
  };
  onRowClick = (record, index) => {
    let selectKey = [index];
    this.setState({
      selectedRowKeys: selectKey,
      selectedItem: record,
    });
  };
  openOrderDetail = () => {
    let item = this.state.selectedItem;
    if (!item) {
      Modal.info({
        title: "信息",
        content: "请先选择一条订单",
      });
      return;
    }
    
  };
  requestList = () => {
    let url = "/order/list";
    axios
      .get(url)
      .then((response) => {
        let result = response.data;
        if (result.success) {
          let list = result.list.map((item, index) => {
            item.key = index;
            return item;
          });
          this.setState({
            list: list,
            selectedRowKeys: [],
          });
        }
      })
      .catch((error) => {});
  };
  handleOpenCity = (item) => {
    this.setState({
      isShowOpenCity: true,
    });
  };
  render() {
    const colums = [
      {
        title: "订单编号",
        dataIndex: "order_sn",
      },
      {
        title: "车辆编号",
        dataIndex: "bike_sn",
      },
      {
        title: "用户名",
        dataIndex: "user_name",
      },
      {
        title: "手机号",
        dataIndex: "mobile",
      },
      {
        title: "里程",
        dataIndex: "distance",
        render(distance) {
          return distance / 1000 + "Km";
        },
      },
      {
        title: "行驶时长",
        dataIndex: "total_time",
      },
      {
        title: "状态",
        dataIndex: "status",
        render(status) {
          return status == 0 ? "未开始" : "已结束";
        },
      },
      {
        title: "开始时间",
        dataIndex: "start_time",
      },
      {
        title: "结束时间",
        dataIndex: "end_time",
      },
      {
        title: "订单金额",
        dataIndex: "total_fee",
      },
      {
        title: "实付金额",
        dataIndex: "user_pay",
      },
    ];
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 },
    };
    const selectedRowKeys = this.state.selectedRowKeys;
    const rowSelection = {
      type: "radio",
      selectedRowKeys,
    };
    return (
      <div style={{ width: 100 + "%" }}>
        <Card>
          <BaseForm formList={this.formList} filterSubmit={this.handleFilter} />
        </Card>
        <Card style={{ marginTop: 10 }}>
          <Button type="primary" onClick={this.openOrderDetail}>
            订单详情
          </Button>
          <Button type="primary" style={{ marginLeft: 10 }}>
            结束订单
          </Button>
        </Card>
        <div className="content-wrap">
          <Table
            bordered
            rowSelection={rowSelection}
            columns={colums}
            dataSource={this.state.list}
            pagination={this.state.pagination}
            onRow={(record, index) => {
              return {
                onClick: () => {
                  this.onRowClick(record, index);
                },
              };
            }}
          ></Table>
        </div>
        <Modal
          title="结束订单"
          visible={this.state.orderConfirmVisble}
          onCancel={() => {
            this.setState({
              orderConfirmVisble: false,
            });
          }}
          onOk={() => {
            this.setState({
              orderConfirmVisble: false,
            });
          }}
          width={600}
        >
          <Form layout="horizontal" {...formItemLayout}>
            <formItem label="车辆编号">{this.state.orderInfo.bike_sn}</formItem>
            <formItem label="剩余电量">
              {this.state.orderInfo.battery + "%"}
            </formItem>
            <formItem label="行程开始时间">
              {this.state.orderInfo.start_time}
            </formItem>
            <formItem label="当前位置">
              {this.state.orderInfo.location}
            </formItem>
          </Form>
        </Modal>
      </div>
    );
  }
}
