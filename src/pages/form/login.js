import React from "react";
import { Form, Input, Button, Checkbox, Card, message } from "antd";
const FormItem = Form.Item;
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};
const layout = {
  wrapperCol: {
    offset: 4,
  },
};
export default class Login extends React.Component {
  constructor(props) {
    super(props);
  }
  handleSubmit = (values) => {
    let userInfo = values;
    message.success(
      `${userInfo.userName} 恭喜你，您通过本次表单组件学习，当前密码为：${userInfo.userPwd}`
    );
  };
  render() {
    return (
      <div>
        <Card title="登录水平表单" style={{ marginTop: 10 }}>
          <Form style={{ width: 300 }} onFinish={this.handleSubmit}>
            <FormItem
              name="userName"
              label="姓名"
              rules={[
                {
                  required: true,
                  message: "用户名不能为空",
                },
                {
                  pattern: new RegExp("^\\w+$", "g"),
                  message: "用户名必须为字母或者数字",
                },
              ]}
            >
              <Input placeholder="请输入用户名" />
            </FormItem>
            <FormItem
              name="userPwd"
              label="密码"
              rules={[
                {
                  required: true,
                  message: "密码不能为空",
                },
              ]}
            >
              <Input type="password" placeholder="请输入密码" />
            </FormItem>

            <FormItem valuePropName="checked" {...layout}>
              <Checkbox>记住密码</Checkbox>
              <a href="#" style={{ float: "right" }}>
                忘记密码
              </a>
            </FormItem>
            <FormItem {...tailLayout}>
              <Button
                type="primary"
                htmlType="submit"
                // onClick={this.handleSubmit}
              >
                保存
              </Button>
              {/* <Button type="primary" onClick={this.handleSubmit}>
                登录
              </Button> */}
            </FormItem>
          </Form>
        </Card>
      </div>
    );
  }
}
