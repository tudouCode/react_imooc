import React from "react";
import {
  Card,
  Form,
  Button,
  Input,
  Checkbox,
  Radio,
  Select,
  Switch,
  DatePicker,
  TimePicker,
  Upload,
  message,
  InputNumber,
} from "antd";

import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import moment from "moment";
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const TextArea = Input.TextArea;

export default class Register extends React.Component {
  constructor(props) {
    super(props);
  }
  state = {
    loading: false,
    imageUrl: [],
  };

  handleSubmit = (values) => {
    let userInfo = values;
    message.success(
      `${userInfo.userName} 恭喜你，您通过本次表单组件学习，当前密码为：${userInfo.userPwd}`
    );
  };
  getBase64 = (img, callBack) => {
    const render = new FileReader();
    render.addEventListener("load", () => callBack(render.result));
    render.readAsDataURL(img);
  };
  handleChange = (info) => {
    if (info.file.status === "uploading") {
      this.setState({ loading: true });
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, (imageUrl) =>
        this.setState({
          userImg: imageUrl,
          loading: false,
        })
      );
    }
  };
  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const offsetLayout = {
      wrapperCol: {
        xs: 24,
        sm: {
          span: 12,
          offset: 4,
        },
      },
    };
    const rowObject = {
      minRows: 4,
      maxRows: 6,
    };
    const uploadButton = (
      <div>
        {this.state.loading ? <LoadingOutlined /> : <PlusOutlined />}
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const { imageUrl } = this.state;

    return (
      <div>
        <Card title="注册表单" style={{ marginTop: 10, width: 800 }}>
          <Form {...formItemLayout} onFinish={this.handleSubmit}>
            <FormItem
              label="用户名"
              name="userName"
              rules={[
                {
                  required: true,
                  message: "用户名不能为空",
                },
                {
                  pattern: new RegExp("^\\w+$", "g"),
                  message: "用户名必须为字母或者数字",
                },
              ]}
            >
              <Input placeholder="请输入用户名" />
            </FormItem>
            <FormItem
              name="userPwd"
              label="密码"
              rules={[
                {
                  required: true,
                  message: "密码不能为空",
                },
              ]}
            >
              <Input type="password" placeholder="请输入密码" />
            </FormItem>

            <FormItem name="sex" label="性别" initialValue={"2"}>
              <RadioGroup>
                <Radio value="1">男</Radio>
                <Radio value="2">女</Radio>
              </RadioGroup>
            </FormItem>
            <FormItem label="年龄" name="age" initialValue={"27"}>
              <InputNumber />
            </FormItem>
            <FormItem label="当前状态" name="state" initialValue={"1"}>
              <Select>
                <Option value="1">咸鱼一条</Option>
                <Option value="2">风华浪子</Option>
                <Option value="3">北大才子一枚</Option>
                <Option value="4">百度FE</Option>
                <Option value="5">创业者</Option>
              </Select>
            </FormItem>
            <FormItem label="爱好" name="interest" initialValue={["1", "8"]}>
              <Select mode="multiple">
                <Option value="1">游泳</Option>
                <Option value="2">打篮球</Option>
                <Option value="3">踢足球</Option>
                <Option value="4">跑步</Option>
                <Option value="5">爬山</Option>
                <Option value="6">骑行</Option>
                <Option value="7">桌球</Option>
                <Option value="8">麦霸</Option>
              </Select>
            </FormItem>
            <FormItem
              label="是否已婚"
              name="isMarried"
              valuePropName={"checked"}
              initialValue={false}
            >
              <Switch />
            </FormItem>
            <FormItem
              label="生日"
              name="birthday"
              initialValue={moment("2018-08-08")}
            >
              <DatePicker showTime format="YYYY-MM-DD HH:mm:ss" />
            </FormItem>
            <FormItem
              label="联系地址"
              name="address"
              initialValue={"武汉市洪山区"}
            >
              <TextArea autosize={rowObject} />
            </FormItem>
            <FormItem label="早起时间" name="time">
              <TimePicker />
            </FormItem>
            <FormItem label="头像" name="userImg">
              <Upload
                name="userImg"
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                onChange={this.handleChange}
              >
                {imageUrl ? (
                  <img src={imageUrl} alt="avatar" style={{ width: "100%" }} />
                ) : (
                  uploadButton
                )}
              </Upload>
            </FormItem>

            <FormItem {...offsetLayout}>
              <Checkbox>
                我已阅读过<a href="#">慕课协议</a>
              </Checkbox>
            </FormItem>
            <FormItem {...offsetLayout}>
              <Button type="primary" htmlType="submit">
                注册
              </Button>
            </FormItem>
          </Form>
        </Card>
      </div>
    );
  }
}
