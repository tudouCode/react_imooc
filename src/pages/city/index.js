import React from "react";
import { Card, Button, Table, Modal } from "antd";
import axios from "axios";
import FilterForm from "../../components/cityForm/cityManageForm";
import OpenCityForm from "./../../components/cityForm/OpenCityForm";
import "./../../mock";
export default class City extends React.Component {
  state = {
    list: [],
    isShowOpenCity: false,
  };
  params = {
    page: 1,
  };
  componentDidMount() {
    this.requestList();
  }
  requestList = () => {
    let url = "/city/TableList";
    axios
      .get(url)
      .then((response) => {
        let result = response.data;
        if (result.success) {
          let list = result.list.map((item, index) => {
            item.key = index;
            return item;
          });
          this.setState({
            list: list,
            selectedRowKeys: [],
          });
        }
      })
      .catch((error) => {});
  };
  handleOpenCity = (item) => {
    this.setState({
      isShowOpenCity: true,
    });
  };
  render() {
    const colums = [
      {
        title: "城市ID",
        dataIndex: "id",
      },
      {
        title: "城市名称",
        dataIndex: "name",
      },
      {
        title: "用车模式",
        dataIndex: "mode",
        render(mode) {
          return mode == 1 ? "停车点" : "禁停区";
        },
      },
      {
        title: "营运模式",
        dataIndex: "op_mode",
        render(op_mode) {
          return op_mode == "1" ? "自营" : "加盟";
        },
      },
      {
        title: "授权加盟商",
        dataIndex: "auth_status",
        render(auth_status) {
          return auth_status == "1" ? "已授权" : "未授权";
        },
      },
      {
        title: "城市管理员",
        dataIndex: "city_admins",
      },
      {
        title: "城市开通时间",
        dataIndex: "open_time",
      },
      {
        title: "操作时间",
        dataIndex: "update_time",
      },
      {
        title: "操作人",
        dataIndex: "sys_user_name",
      },
    ];

    const rowSelection = {
      type: "radio",
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRows,
          selectedRowKeys,
        });
      },
    };
    return (
      <div style={{ width: 100 + "%" }}>
        <Card>
          <FilterForm />
        </Card>
        <Card style={{ marginTop: 10 }}>
          <Button type="primary" onClick={this.handleOpenCity}>
            开通城市
          </Button>
        </Card>
        <div className="content-wrap">
          <Table
            bordered
            rowSelection={rowSelection}
            columns={colums}
            dataSource={this.state.list}
            pagination={this.state.pagination}
          ></Table>
        </div>
        <Modal
          title="开通城市"
          visible={this.state.isShowOpenCity}
          onCancel={() => {
            this.setState({
              isShowOpenCity: false,
            });
          }}
          onOk={() => {
            this.setState({
              isShowOpenCity: false,
            });
          }}
        >
          <OpenCityForm selectRow={this.state.selectedRows} />
        </Modal>
      </div>
    );
  }
}
