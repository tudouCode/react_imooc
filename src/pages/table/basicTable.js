import React from "react";
import { Card, Table, Modal, Button, message } from "antd";
import axios from "axios";
// import Utils from "./../../utils/utils";
import "./../../mock";
export default class BasicTable extends React.Component {
  state = {
    dataSource2: [],
  };
  // dom加载完以后执行
  componentDidMount() {
    const data = [
      {
        id: "0",
        userName: "Jack",
        sex: "1",
        state: "1",
        interest: "1",
        birthday: "2000-01-01",
        address: "北京市海淀区奥林匹克公园",
        time: "09:00",
      },
      {
        id: "1",
        userName: "Tom",
        sex: "1",
        state: "1",
        interest: "1",
        birthday: "2000-01-01",
        address: "北京市海淀区奥林匹克公园",
        time: "09:00",
      },
      {
        id: "2",
        userName: "Lily",
        sex: "1",
        state: "1",
        interest: "1",
        birthday: "2000-01-01",
        address: "北京市海淀区奥林匹克公园",
        time: "09:00",
      },
    ];
    data.map((item, index) => {
      item.key = index;
    });
    this.setState({
      dataSource: data,
    });
    this.request();
  }
  // 动态获取数据
  request = () => {
    let url = "/list";
    axios
      .get(url)
      .then((response) => {
        let result = response.data;
        if (result.success) {
          console.log(result);
          result.list.map((item, index) => {
            item.key = index;
          });
          this.setState({
            dataSource2: result.list,
            selectedRowKeys: [],
          });
        }
      })
      .catch((error) => {});
  };
  handleDelete = () => {
    console.log(this.state.selectedRows);
    let rows = this.state.selectedRows;
    let ids = [];
    rows.map((item) => {
      ids.push(item.id);
    });
    Modal.confirm({
      title: "删除提示",
      content: `您确定要删除这些数据吗？${ids.join(",")}`,
      onOk: () => {
        message.success("删除成功");
        this.request();
      },
    });
  };
  render() {
    const columns = [
      {
        title: "id",
        key: "id",
        dataIndex: "id",
      },
      {
        title: "用户名",
        key: "userName",
        dataIndex: "userName",
      },
      {
        title: "性别",
        key: "sex",
        dataIndex: "sex",
        render(sex) {
          return sex == 1 ? "男" : "女";
        },
      },
      {
        title: "状态",
        key: "state",
        dataIndex: "state",
        render(state) {
          let config = {
            1: "咸鱼一条",
            2: "风华浪子",
            3: "北大才子",
            4: "百度FE",
            5: "创业者",
          };
          return config[state];
        },
      },
      {
        title: "爱好",
        key: "interest",
        dataIndex: "interest",
        render(abc) {
          let config = {
            1: "游泳",
            2: "打篮球",
            3: "踢足球",
            4: "跑步",
            5: "爬山",
            6: "骑行",
            7: "桌球",
            8: "麦霸",
          };
          return config[abc];
        },
      },
      {
        title: "生日",
        key: "birthday",
        dataIndex: "birthday",
      },
      {
        title: "地址",
        key: "address",
        dataIndex: "address",
      },
      {
        title: "早起时间",
        key: "time",
        dataIndex: "time",
      },
    ];
    const selectedRowKeys = this.state.selectedRowKeys;

    const rowCheckSelection = {
      type: "checkbox",
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys,
          selectedRows,
        });
      },
    };
    return (
      <div style={{ width: 100 + "%" }}>
        <Card title="基础表格">
          <Table
            bordered
            columns={columns}
            dataSource={this.state.dataSource}
          ></Table>
        </Card>
        <Card title="动态表格" style={{ margin: "10px 0" }}>
          <div style={{ marginBottom: 10 }}>
            <Button onClick={this.handleDelete}>删除</Button>
          </div>
          <Table
            bordered
            rowSelection={rowCheckSelection}
            columns={columns}
            dataSource={this.state.dataSource2}
          ></Table>
        </Card>
      </div>
    );
  }
}
