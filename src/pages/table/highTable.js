import React from "react";
import { Card, Table, Modal, Button, message, Space } from "antd";
import axios from "axios";
import "../../mock";
export default class TableHigh extends React.Component {
  state = {
    dataSource2: [],
  };
  params = {
    page: 1,
  };
  // dom加载完以后执行
  componentDidMount() {
    this.request();
  }
  // 动态获取数据
  request = () => {
    let url = "/high/TableList";
    axios
      .get(url)
      .then((response) => {
        let result = response.data;
        if (result.success) {
          result.list.map((item, index) => {
            item.key = index;
          });
          this.setState({
            dataSource2: result.list,
          });
        }
      })
      .catch((error) => {});
  };
  handleDelete = (item) => {
    let id = item.id;
    Modal.confirm({
      title: "删除提示",
      content: `您确定要删除这些数据吗？${id}`,
      onOk: () => {
        message.success("删除成功");
        this.request();
      },
    });
  };
  handleAdd = (item) => {
    let id = item.id;
    Modal.confirm({
      title: "添加提示",
      content: `您确定要添加这些数据吗？${id}`,
      onOk: () => {
        message.success("添加成功");
        this.request();
      },
    });
  };
  render() {
    const columns = [
      {
        title: "id",
        key: "id",
        dataIndex: "id",
        width: 80,
      },
      {
        title: "用户名",
        key: "userName",
        dataIndex: "userName",
        width: 80,
      },
      {
        title: "性别",
        key: "sex",
        width: 80,
        dataIndex: "sex",
        render(sex) {
          return sex == 1 ? "男" : "女";
        },
      },
      {
        title: "状态",
        key: "state",
        dataIndex: "state",
        width: 80,
        render(state) {
          let config = {
            1: "咸鱼一条",
            2: "风华浪子",
            3: "北大才子",
            4: "百度FE",
            5: "创业者",
          };
          return config[state];
        },
      },
      {
        title: "爱好",
        key: "interest",
        width: 80,
        dataIndex: "interest",
        render(abc) {
          let config = {
            1: "游泳",
            2: "打篮球",
            3: "踢足球",
            4: "跑步",
            5: "爬山",
            6: "骑行",
            7: "桌球",
            8: "麦霸",
          };
          return config[abc];
        },
      },
      {
        title: "生日",
        key: "birthday",
        width: 120,
        sorter: (a, b) => {
          return a.age - b.age;
        },
        dataIndex: "birthday",
      },
      {
        title: "地址",
        key: "address",
        width: 120,
        dataIndex: "address",
      },
      {
        title: "早起时间",
        key: "time",
        width: 80,
        dataIndex: "time",
      },
      {
        title: "操作",
        render: (text, item) => (
          <Space size="small">
            <Button
              size="small"
              onClick={() => {
                this.handleDelete(item);
              }}
            >
              删除
            </Button>
            <Button
              size="small"
              onClick={() => {
                this.handleAdd(item);
              }}
            >
              添加
            </Button>
          </Space>
        ),
      },
    ];
    const selectedRowKeys = this.state.selectedRowKeys;

    const rowCheckSelection = {
      type: "checkbox",
      selectedRowKeys,
      onChange: (selectedRowKeys, selectedRows) => {
        this.setState({
          selectedRowKeys,
          selectedRows,
        });
      },
    };
    return (
      <div style={{ width: 100 + "%" }}>
        <Card title="动态表格" style={{ margin: "10px 0" }}>
          <Table
            bordered
            rowSelection={rowCheckSelection}
            columns={columns}
            dataSource={this.state.dataSource2}
          ></Table>
        </Card>
      </div>
    );
  }
}
