const { createProxyMiddleware } = require("http-proxy-middleware");
module.exports = function (app) {
  app.use(
    createProxyMiddleware("/weatherUrl", { target: "http://api.map.baidu.com" })
  );
};
