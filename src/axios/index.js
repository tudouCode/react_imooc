import request from "./ajax1";
export function getPlanListByPage(query) {
  return request({
    url: "/table/list",
    method: "GET",
    data: query,
  });
}

