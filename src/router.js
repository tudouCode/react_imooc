import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import App from "./App";
import Admin from "./admin";
import Home from "./pages/home";
import Buttons from "./pages/ui/buttons";
import Modals from "./pages/ui/modals";
import Loadings from "./pages/ui/loadings";
import Notification from "./pages/ui/notification";
import Msg from "./pages/ui/messages";
import Tabs from "./pages/ui/tabs";
import FormLogin from "./pages/form/login";
import Register from "./pages/form/register";
import TableBasic from "./pages/table/basicTable";
import TableHigh from "./pages/table/highTable";
import cityManage from "./pages/city";
import Order from "./pages/order";
import NoMatch from "./pages/nomatch";
// import Common from "./common";
export default class ERouter extends React.Component {
  render() {
    return (
      <HashRouter>
        <App>
          <Switch>
            <Route
              path="/"
              render={() => (
                <Admin>
                  <Switch>
                    <Route path="/home" component={Home} />
                    <Route path="/ui/buttons" component={Buttons} />
                    <Route path="/ui/modals" component={Modals} />
                    <Route path="/ui/loadings" component={Loadings} />
                    <Route path="/ui/notification" component={Notification} />
                    <Route path="/ui/messages" component={Msg} />
                    <Route path="/ui/tabs" component={Tabs} />
                    <Route path="/form/login" component={FormLogin} />
                    <Route path="/form/register" component={Register} />
                    <Route path="/table/basic" component={TableBasic} />
                    <Route path="/table/high" component={TableHigh} />
                    <Route path="/city" component={cityManage} />
                    <Route path="/order" component={Order} />
                    <Route component={NoMatch} />
                  </Switch>
                </Admin>
              )}
            />
          </Switch>
        </App>
      </HashRouter>
    );
  }
}
