import React from "react";
import { Button, Form, Select, Input } from "antd";
const FormItem = Form.Item;
const Option = Select.Option;
export default class OpenCityForm extends React.Component {
  // dom 加载完执行的函数
  componentDidMount = () => {
    let selectRowData = this.props.selectRow[0];
    this.getCommonFun(selectRowData);
  };
  getCommonFun = (params) => {
    let selectRowData = params;
    this.refs.addForm.setFieldsValue(selectRowData);
    this.refs.addForm.setFieldsValue({ mode: String(selectRowData.mode) });
    this.refs.addForm.setFieldsValue({
      auth_status: String(selectRowData.auth_status),
    });
    this.refs.addForm.setFieldsValue({
      op_mode: String(selectRowData.op_mode),
    });
  };
  // 实时获取父组件改变的值
  componentWillReceiveProps(props) {
    let selectRowData = props.selectRow[0];
    this.getCommonFun(selectRowData);
  }
  render() {
    const formItemLayout = {
      labelCol: {
        span: 5,
      },
      wrapperCol: {
        span: 19,
      },
    };
    return (
      <Form ref="addForm" layout="horizontal" {...formItemLayout}>
        <FormItem label="操作人" name="sys_user_name">
          <Input placeholder="请输入城市序号" />
        </FormItem>
        <FormItem label="用车模式" name="mode">
          <Select placeholder="全部">
            <Option value="0">全部</Option>
            <Option value="1">指定停车点模式</Option>
            <Option value="2">禁停区模式</Option>
          </Select>
        </FormItem>
        <FormItem label="营运模式" name="op_mode">
          <Select placeholder="全部">
            <Option value="0">全部</Option>
            <Option value="1">自营</Option>
            <Option value="2">加盟</Option>
          </Select>
        </FormItem>
        <FormItem label="授权状态" name="auth_status">
          <Select placeholder="全部">
            <Option value="0">全部</Option>
            <Option value="1">已授权</Option>
            <Option value="2">未授权</Option>
          </Select>
        </FormItem>
      </Form>
    );
  }
}
