import React from "react";
import { Row, Col } from "antd";
import "./index.less";
import Util from "../../utils/index";
// import { Axios } from "../../axios/index";
import axios from "../../axios/ajax";
import { connect } from "react-redux"; //链接
import { switchMenu } from "../../redux/action"; //连接器
import menuConfig from "./../../resource/menuConfig";
class Header extends React.Component {
  state = {};
  componentWillMount() {
    this.setState({
      userName: "北葵向暖",
    });
    setInterval(() => {
      let sysTime = Util.formateDate(new Date().getTime());
      this.setState({
        sysTime,
      });
    }, 1000);
    // this.getWeatherAPIData();
  }
  componentDidMount() {
    this.handleMenUpdate(menuConfig);
  }
  // 页面刷新时 也实时获取当前头部数据
  handleMenUpdate = (data) => {
    console.log(data);
    let currentKey = window.location.hash.replace(/#|\?.*$/g, "");
    const { dispatch } = this.props;
    let obj = [];
    data.map((item) => {
      if (item.children) {
        // 如果有children属性,将其展开放入数组中
        obj.push(...item.children);
      } else {
        obj.push(item);
      }
    });
    const menuName = obj;
    menuName.forEach((item) => {
      if (currentKey == item.key) {
        dispatch(switchMenu(item.title));
      }
    });
  };
  getWeatherAPIData() {
    let city = "武汉市";
    let url =
      "http://api.map.baidu.com/telematics/v3/weather?location=" +
      encodeURIComponent(city) +
      "&output=json&ak=3p49MVra6urFRGOT9s8UBWr2";
    axios
      .jsonp({
        url: url,
      })
      .then((res) => {
        if (res.status == "success") {
          let data = res.results[0].weather_data[0];
          this.setState({
            dayPictureUrl: data.dayPictureUrl,
            weather: data.weather,
          });
        }
      });
  }
  render() {
    const { menuName } = this.props;
    return (
      <div className="header">
        <Row className="header-top">
          <Col span="24">
            <span>欢迎,{this.state.userName}</span>
            <a href="#!">退出</a>
          </Col>
        </Row>
        <Row className="breadcrumb">
          <Col span="4" className="breadcrumb-title">
            {menuName}
          </Col>
          <Col span="20" className="weather">
            <span className="date">{this.state.sysTime}</span>
            <span className="weather-img">
              <img src={this.state.dayPictureUrl} alt="天气图片" />
            </span>
            <span className="weather-detail">{this.state.weather}</span>
          </Col>
        </Row>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    menuName: state.menuName,
  };
};
export default connect(mapStateToProps)(Header);
