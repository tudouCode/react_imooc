import React from "react";
import { Menu } from "antd";
import "./index.less";
import menuConfig from "./../../resource/menuConfig";
import { NavLink } from "react-router-dom";
import imgTitle from "../../resource/assets/logo-ant.svg";
import { connect } from "react-redux"; //链接
import { switchMenu } from "./../../redux/action";
const SubMenu = Menu.SubMenu;
class NavLeft extends React.Component {
  state = {
    currentKey: "",
  };
  componentWillMount() {
    let currentKey = window.location.hash.replace(/#|\?.*$/g, "");
    const MenuTreeNode = this.renderMenu(menuConfig);
    // 保存值
    this.setState({
      currentKey,
      MenuTreeNode,
    });
  }
  handleClick = ({ item, key }) => {
    // 事件派发
    const { dispatch } = this.props;
    dispatch(switchMenu(item.props.title));
    this.setState({
      currentKey: key,
    });
  };
  // 菜单渲染
  renderMenu = (data) => {
    return data.map((item) => {
      if (item.children) {
        return (
          <SubMenu title={item.title} key={item.key}>
            {this.renderMenu(item.children)}
          </SubMenu>
        );
      }
      return (
        <Menu.Item title={item.title} key={item.key}>
          <NavLink to={item.key}>{item.title}</NavLink>
        </Menu.Item>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="logo">
          <img src={imgTitle} alt="" />
          <h1>管理系统</h1>
        </div>
        {/* 高亮 */}
        <Menu
          theme="dark"
          onClick={this.handleClick}
          selectedKeys={[this.state.currentKey]}
        >
          {this.state.MenuTreeNode}
        </Menu>
      </div>
    );
  }
}
export default connect()(NavLeft);
