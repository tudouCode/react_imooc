import React from "react";
import { Button, Form, Select, DatePicker, Checkbox, Input } from "antd";
import Utils from "../../utils";
const FormItem = Form.Item;

export default class BaseForm extends React.Component {
  initFormList = () => {
    const formList = this.props.formList;

    const formItemList = [];
    if (formList && formList.length > 0) {
      formList.forEach((item, i) => {
        let label = item.label;
        let field = item.field;
        let initialValue = item.initialValue || "";
        let placeholder = item.placeholder;
        let width = item.width;
        if (item.type == "时间查询") {
          const begin_time = (
            <FormItem
              label="订单时间"
              name="startTime"
              key="startTime"
              initialValue={initialValue}
            >
              <DatePicker
                showTime={true}
                placeholder={placeholder}
                format="YYYY-MM-DD HH:mm:ss"
              />
            </FormItem>
          );
          formItemList.push(begin_time);
          const end_time = (
            <FormItem
              label="~"
              colon={false}
              key="endTime"
              name="endTime"
              name={field}
              initialValue={initialValue}
            >
              <DatePicker
                showTime={true}
                placeholder={placeholder}
                format="YYYY-MM-DD HH:mm:ss"
              />
            </FormItem>
          );
          formItemList.push(end_time);
        } else if (item.type == "INPUT") {
          const INPUT = (
            <FormItem
              name="searchContent"
              label={label}
              key={field}
              initialValue={initialValue}
            >
              <Input type="text" placeholder={placeholder} />
            </FormItem>
          );
          formItemList.push(INPUT);
        } else if (item.type == "SELECT") {
          const SELECT = (
            <FormItem
              name={label + "val"}
              label={label}
              key={field}
              initialValue={initialValue}
            >
              <Select style={{ width: width }} placeholder={placeholder}>
                {Utils.getOptionList(item.list)}
              </Select>
            </FormItem>
          );
          formItemList.push(SELECT);
        } else if (item.type == "CHECKBOX") {
          const CHECKBOX = (
            <FormItem label={label} key={field} initialValue={initialValue}>
              <Checkbox>{label}</Checkbox>
            </FormItem>
          );
          formItemList.push(CHECKBOX);
        }
      });
    }
    return formItemList;
  };
  handleFilterSubmit = () => {
    let fieldsValue = this.refs.baseForm.getFieldsValue();
    this.props.filterSubmit(fieldsValue);
  };
  reset = () => {
    this.refs.baseForm.resetFields();
  };
  render() {
    return (
      <Form layout="inline" ref="baseForm">
        {this.initFormList()}
        <FormItem>
          <Button
            type="primary"
            style={{ margin: "0 20px" }}
            onClick={this.handleFilterSubmit}
          >
            查询
          </Button>
          <Button onClick={this.reset}>重置</Button>
        </FormItem>
      </Form>
    );
  }
}
