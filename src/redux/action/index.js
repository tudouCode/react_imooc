// 触发行为
/*
 * action 类型
 */
export const type = {
  SWITCH_MENU: "SWITCH_MENU",
};

// 方法
export function switchMenu(menuName) {
  return {
    type: type.SWITCH_MENU,
    menuName,
  };
}

